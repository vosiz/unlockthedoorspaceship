﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public MainIniFile main_ini;
    public Version version;

    // Start is called before the first frame update
    void Start()
    {

        main_ini = new MainIniFile();

        version = new Version();
        //Debug.Log("Version: " + version.GetFullVersion());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
