﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn {

    // Spawns item on map
    static public GameObject SpawnItem(string name, string path, Vector3 location, Vector3 rotation) {

        string object_path = path + name;

        GameObject obj = (GameObject)Object.Instantiate(
            Resources.Load(object_path),
            location,
            Quaternion.Euler(rotation));

        //Debug.Log(string.Format("Spawning {0} at {1} with rot {2}", name, location, rotation));

        obj.name = name;

        return obj;
    }

    // Tries to spawn item in location
    // if cannot be spawned due to range limitation of map obejcts
    // tries X times, then give up
    static public GameObject TrySpawn(string name, string path, Vector3 min_location, Vector3 max_location, float range_limitation, Vector3 rotation, List<GameObject> map_objects, int attempts = 1) {

        for (int i = 0; i < attempts; i++) {

            Vector3 location = VectorUtils.RandomLocation(min_location, max_location);
            bool reset = false;

            foreach (GameObject go in map_objects) {

                if (VectorUtils.IsInRange(location, go.transform.position, range_limitation)) {

                    // object standing in way, try next random location
                    reset = true;
                    break;
                }
            }

            if (reset) continue;

            return SpawnItem(name, path, location, rotation);
        }

        Debug.Log("Cannot spawn object: " + name);

        return null;
    }

    // Plays random sound from given infomation (sound00 to sound## - name mutations)
    // When name mutation is 0, plays exact name
    static public void PlaySound(string name, string path, int name_mutations, Vector3 location, float volume = 1.0f) {

        AudioClip source;

        if (name_mutations != 0){

            string mutation_ext = string.Format("{0:00}", Mathf.RoundToInt(Random.Range(0, name_mutations)));
            source = Resources.Load(path + name + mutation_ext) as AudioClip;

        } else {

            source = Resources.Load(path + name) as AudioClip;
        }

        //Debug.Log("Trying to play sound " + path + name);

        Assert.IsNull(source);
        AudioSource.PlayClipAtPoint(source, location, volume);
    }
}
