﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Assert
{

    // Tries if given object is not null
    // if it is, throw exception
    // - o: tested object
    internal static void IsNull(object o)
    {

        if (o == null)
        {

            Debug.Log("ASSERT: Tested object is null !!!");
            throw new System.Exception("Assertation failed, object is null");
        }
    }

    // Assert on condition
    // if true, then throw exception
    // condition: result of condition
    internal static void Cond(bool condition)
    {

        if (condition)
        {

            Debug.Log("ASSERT: Contition fulfilled !!!");
            throw new System.Exception("Assertation failed, condition fulfiled");
        }

    }
}
