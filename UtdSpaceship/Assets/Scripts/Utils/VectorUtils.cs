﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtils
{

    // Move towards point from start to end with speed
    public static void MoveTowardsPoint(GameObject go, Vector3 destination, float speed){

        float step = speed * Time.deltaTime;

        go.transform.position = Vector3.MoveTowards(go.transform.position, destination, step);
    }

    // Rotates object towards destination with given speed
    public static void RotateTowardsPoint(GameObject go, Vector3 dest, float speed) {

        Vector3 now_direction = dest - go.transform.position;

        float step = speed * Time.deltaTime;
        Vector3 dest_direction = Vector3.RotateTowards(go.transform.forward, now_direction, step, 0.0f);
        go.transform.rotation = Quaternion.LookRotation(dest_direction);
    }

    // Returns random rotation
    public static Vector3 RandomRotation() {

        return RandomRotation(Vector3.one * 360.0f);
    }

    // Returns random rotation with maximum defined
    public static Vector3 RandomRotation(Vector3 max) {

        return RandomRotation(Vector3.zero, max);
    }

    // Returns random rotation with min and max defined
    public static Vector3 RandomRotation(Vector3 min, Vector3 max) {

        return new Vector3(
                Random.Range(min.x, max.x),
                Random.Range(min.y, max.y),
                Random.Range(min.z, max.z));
    }

    // Returns random location with min and max defined
    public static Vector3 RandomLocation(Vector3 min, Vector3 max) {

        return RandomRotation(min, max);
    }

    // Returns random location with max defined
    public static Vector3 RandomLocation(Vector3 max) {

        return RandomRotation(max);
    }


    // Returns if object is in range
    public static bool IsInRange(Vector3 location, Vector3 target, float range) {

        return Vector3.Distance(location, target) < range;
    }

    // Returns direction vector between 2 points
    public static Vector3 Direction(Vector3 start, Vector3 end) {

        return (end - start).normalized;
    }

    // Rotate given game object towards point in world
    public static void RotateTo(GameObject go, Vector3 world_point) {

        go.transform.rotation =
            Quaternion.LookRotation(
                Direction(go.transform.position, world_point));
    }
}
