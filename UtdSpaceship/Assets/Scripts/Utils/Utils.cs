﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{
    // Returns GameObject with name in children list of given GameObject
    // go: parent gameobject
    // name: name of children you are looking for
    static public GameObject GetGameObjectByName(GameObject go, string name)
    {

        //Author: Isaac Dart, June-13.
        Transform[] ts = go.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
            if (t.gameObject.name == name)
                return t.gameObject;

        return null;
    }

    // Returns GameObjects with name in children list of given GameObject
    static public List<GameObject> GetGameObjectsByName(GameObject go, string name)
    {

        List<GameObject> found;
        found = new List<GameObject>();

        Transform[] ts = go.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
            if (t.gameObject.name == name)
                found.Add(t.gameObject);

        return found;
    }
}
