﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class Alg
{

    // Shakes target camera with given options
    static public IEnumerator CameraShake(GameObject target, float duration, float magnitude){

        Vector3 original_position = target.transform.localPosition;
        Quaternion original_rotation = target.transform.localRotation;
        float timer = 0.0f;

        while(timer < duration){

            float perc = timer / duration;

            if (perc == 0.0f){

                perc = 1;
            }

            // random position
            float x = Random.Range(-1/perc, 1/perc) * magnitude / 100;
            float y = Random.Range(-1/perc, 1/perc) * magnitude / 100;
            target.transform.localPosition = new Vector3(x, y, original_position.z);
             
            /*
            // random rotation
            x = Random.Range(-1 / perc, 1 / perc) * magnitude / 100;
            y = Random.Range(-1 / perc, 1 / perc) * magnitude / 100;
            float z = Random.Range(-1 / perc, 1 / perc) * magnitude / 100;
            target.transform.RotateAround(original_position, new Vector3(x, y, z), 1);
            */

            timer += Time.deltaTime;

            yield return null;
        }

        target.transform.localPosition = original_position;
        target.transform.localRotation = original_rotation;
    }

}
