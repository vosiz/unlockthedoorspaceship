﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatcherSceneManager : MonoBehaviour
{

    public bool caught;
    public bool game_running;

    float dot_timer;
    float blink_timer;

    // Target
    GameObject target;
    Vector3 destination;
    Vector3 rotation;
    float speed;
    float dot_frequency;
    float blink_frequency;
    float screen_offset;

    GameManager gm;

    GameObject catcher_hud;
    public GameObject crosshair;
    CatcherGui cgui;
    

    // Start is called before the first frame update
    void Start()
    {
        caught = false;
        game_running = false;

        target = GameObject.FindGameObjectWithTag("CatcherTarget");
        Assert.IsNull(target);

        catcher_hud = GameObject.FindGameObjectWithTag("CatcherHud");
        Assert.IsNull(catcher_hud);

        cgui = catcher_hud.GetComponent<CatcherGui>();
        Assert.IsNull(cgui);

        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        Assert.IsNull(gm);
        speed = gm.main_ini.catcher.target_speed;
        dot_frequency = gm.main_ini.catcher.dot_frequency;
        blink_frequency = gm.main_ini.catcher.target_blink_freq;
        screen_offset = gm.main_ini.catcher.target_screen_offset;

        dot_timer = 0.0f;
        blink_timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(game_running) {

            MoveTarget();
            SpawnRedDot();

            //TargetBlink();
            cgui.HideArrows();
            CheckArrows();
            CheckTargeting();

            CheckEndGame();
        }
    }


    // Restarts fiven game
    public void RestartGame(){

        caught = false;
        game_running = true;

        target.transform.position = new Vector3(
            Random.Range(screen_offset, Screen.width - screen_offset), 
            Screen.height + screen_offset,
            0.0f);

        destination = new Vector3(Random.Range(screen_offset, Screen.width- screen_offset), -screen_offset, 0);

        // rotation towards destionation
        float x = target.transform.position.x - destination.x;
        float y = Screen.height + 2 * screen_offset;
        rotation = new Vector3(0, 0, Mathf.Atan2(x,y));
        rotation = -rotation * Mathf.Rad2Deg;
        target.transform.rotation = Quaternion.Euler(rotation);

        GameObject[] dots = Utils.GetGameObjectsByName(catcher_hud, "RedDot").ToArray();
        foreach(GameObject dot in dots){

            Destroy(dot);
        }

        cgui.ShowOverlayer(false);
    }

    // Moves target down the screen
    private void MoveTarget(){

        if(!caught){

            VectorUtils.MoveTowardsPoint(target, destination, speed);
        }
    }

    // Blinks target
    private void TargetBlink(){

        blink_timer += Time.deltaTime;

        if(blink_timer > blink_frequency){

            target.SetActive(!target.activeSelf);
            blink_timer -= blink_frequency;
        }
    }

    // If time is right, spawn a trajectory dot
    private void SpawnRedDot(){

        dot_timer += Time.deltaTime;

        if(dot_timer > dot_frequency){

            dot_timer -= dot_frequency;
            GameObject dot = Spawn.SpawnItem("RedDot", "GUI/", Vector3.zero, Vector3.zero);
            dot.transform.position = target.transform.position;
            dot.transform.SetParent(catcher_hud.transform);
        }
    }


    // Check if game is over, in either good or bad way
    private void CheckEndGame(){

        if(target.transform.position == destination){

            GameOver();
        }
    }

    // GameOver, bad
    private void GameOver(){

        game_running = false;
        cgui.ShowOverlayer(true);
    }

    // GameOver, good
    private void GameFinished(){

        game_running = false;
        caught = true;
    }


    // Checks if the target is in crosshair
    private void CheckTargeting(){

        if(VectorUtils.IsInRange(target.transform.position, crosshair.transform.position, 20.0f)){

            cgui.SetTargetingState(CatcherTargetState.TARGETING);
            cgui.HideArrows();

        } else {

            cgui.SetTargetingState(CatcherTargetState.NORMAL);
        }
    }

    // Check position of target and crossahair, enables aiming arrows
    private void CheckArrows(){

        if (target.transform.position.x < crosshair.transform.position.x) {

            cgui.ShowLeftArrow();

        } else {

            cgui.ShowRightArrow();
        }

        if (target.transform.position.y > crosshair.transform.position.y) {

            cgui.ShowUpArrow();

        } else {

            cgui.ShowDownArrow();
        }
    }

    // Attempt to catch
    public void Shoot(){

        if (cgui.targeting) {

            GameFinished();
            cgui.SetTargetingState(CatcherTargetState.CAUGHT);
        }
    }
}
