﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ShipMode {

    IDLE,
    RADAR,
    CONFLICT,
    ALERT
}

public class ConflictSceneManager : MonoBehaviour
{

    public ShipMode shipmode;
    public bool controls;
    public bool weapons;

    GameObject hud;
    ConflictGui cgui;

    float shoot_timer;
    bool ammo_ready;
    Ammo used_ammo;

    // Start is called before the first frame update
    void Start()
    {

        hud = GameObject.FindGameObjectWithTag("Hud");
        Assert.IsNull(hud);
        cgui = hud.GetComponent<ConflictGui>();
        Assert.IsNull(cgui);

        shipmode = ShipMode.IDLE;
        ChangeShipMode(shipmode);

        shoot_timer = 0.0f;
        ammo_ready = true;
        used_ammo = new PhotonTorpedo();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!ammo_ready){

            ReloadWeapon();
        }

    }

    private void ReloadWeapon(){

        shoot_timer += Time.deltaTime;
        cgui.weapons_sys.text = "Reloading";

        if (shoot_timer > used_ammo.reload_time) {

            shoot_timer = 0.0f;
            ammo_ready = true;
            cgui.weapons_sys.text = "Active";
        }
    }

    // Restarts conflict scene
    public void Restart(){

        ChangeShipMode(ShipMode.RADAR);
    }

    // Changes ship mode
    public void ChangeShipMode(ShipMode sm) {

        switch (sm) {

            case ShipMode.ALERT:

                cgui.engines.color = AppConfig.colors.TEXT_GUI_ACTIVE;
                cgui.mode.color = AppConfig.colors.TEXT_GUI_ALERT;
                cgui.weapons_sys.color = AppConfig.colors.TEXT_GUI_DISABLED;
                cgui.engines.text = "Active";
                cgui.mode.text = "Alert";
                cgui.weapons_sys.text = "No target";

                controls = true;
                weapons = false;

                break;

            case ShipMode.CONFLICT:

                cgui.engines.color = AppConfig.colors.TEXT_GUI_ACTIVE;
                cgui.mode.color = AppConfig.colors.TEXT_GUI_ALERT;
                cgui.weapons_sys.color = AppConfig.colors.TEXT_GUI_ACTIVE;
                cgui.engines.text = "Active";
                cgui.mode.text = "Combat";
                cgui.weapons_sys.text = "Ready";

                controls = true;
                weapons = true;
                ammo_ready = false;

                break;

            case ShipMode.IDLE:
            case ShipMode.RADAR:

                cgui.engines.color = AppConfig.colors.TEXT_GUI_DISABLED;
                cgui.mode.color = AppConfig.colors.TEXT_GUI_NEUTRAL;
                cgui.weapons_sys.color = AppConfig.colors.TEXT_GUI_NEUTRAL;
                cgui.engines.text = "Not active";
                cgui.mode.text = "Idle";
                cgui.weapons_sys.text = "Disabled";

                controls = false;
                weapons = false;
                ammo_ready = false;

                if(sm == ShipMode.RADAR){

                    for(int i = 0; i < 1; i++){

                        Vector3 location = new Vector3(
                            0 + i * 50,
                            20 + i * 10,
                            60);

                        GameObject go = Spawn.SpawnItem("EnemyShip", "Models/", location, Vector3.zero);
                    }

                    ChangeShipMode(ShipMode.CONFLICT);
                }

                break;

            default:
                throw new System.Exception("Unknown shipmode switch: " + sm.ToString());
        }

        cgui.ShowCrosshair(weapons);
    }

    // Shoots photon torpedo
    public void Shoot(){

        if(ammo_ready){

            Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

            float x = Screen.width / 2;
            float y = Screen.height / 2;

            Ray ray = cam.ScreenPointToRay(new Vector3(x, y, 0));

            Vector3 rotation = cam.transform.rotation.eulerAngles;
            Vector3 position = ray.origin + cam.transform.forward * 5;



            Spawn.PlaySound(used_ammo.shot_name, used_ammo.shot_path, 0, gameObject.transform.position);

            GameObject projectile = Spawn.SpawnItem("PhotonTorpedo", "Models/Projectile/", position, rotation);
            projectile.GetComponent<AmmoMotor>().SetAmmo(used_ammo);

            ammo_ready = false;
        }
    }
}
