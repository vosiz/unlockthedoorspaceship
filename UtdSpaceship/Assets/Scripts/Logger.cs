﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Logger
{
    public static void Debug(bool enable, object message)
    {
        if (enable) UnityEngine.Debug.Log(message);
    }

    public static void Debug(bool enable, object message, Object context) {

        if (enable) UnityEngine.Debug.Log(message, context);
    }
}
