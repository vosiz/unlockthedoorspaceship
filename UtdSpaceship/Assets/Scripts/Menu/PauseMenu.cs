﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    bool menu_opened;
    GameObject pause_menu;
    GameObject main_menu;
    

    // Start is called before the first frame update
    void Start()
    {
        menu_opened = false;
        pause_menu = gameObject;
        main_menu = Utils.GetGameObjectByName(pause_menu, "MainMenu");
        Assert.IsNull(main_menu);
    }

    // Update is called once per frame
    void Update()
    {
        CheckButton();
    }

    // Chesks if button (Escape) was pressed 
    // if not do nothing 
    // if pressed open of close pause menu GUI 
    private void CheckButton()
    {
        bool key_pressed = Input.GetKeyDown(KeyCode.Escape);

        if (key_pressed)
        {
            ShowPauseMenu(!menu_opened);
            Debug.Log("tlačítko stisknuto");
        }

    }
    // Opens or closed pause menu GUI
    // - open : open or close GUI
    private void ShowPauseMenu(bool open)
    {
        main_menu.SetActive(open);
        menu_opened = open;
    }

}

