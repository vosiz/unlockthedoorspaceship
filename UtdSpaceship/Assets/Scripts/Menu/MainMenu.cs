﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    bool DEBUG_BUTTON_CLICK = false;


    private void Update()
    {
        CheckButtons();
    }


    // Checks if there were buttons pressed to change scene according to button value
    // 
    private void CheckButtons()
    {
        bool key_1 = Input.GetKeyDown(KeyCode.Alpha1);
        bool key_2 = Input.GetKeyDown(KeyCode.Alpha2);
        bool key_3 = Input.GetKeyDown(KeyCode.Alpha3);
        bool key_4 = Input.GetKeyDown(KeyCode.Alpha4);

        if (key_1)
        {
            StartHyperspace();
        }
        if (key_2)
        {
            StartFlight();
        }
        if (key_3)
        {
            StartCatcher();
        }
        if (key_4)
        {
            StartConflict();
        }


    }  

// Opens HyperSpace scene
public void StartHyperspace ()
    {
        Logger.Debug(DEBUG_BUTTON_CLICK, "StartHyperspace scene change");
        SceneManager.LoadScene("Hyperspace");
    }

    // Opens StartFlight scene
    public void StartFlight ()
    {
        Logger.Debug(DEBUG_BUTTON_CLICK, "StartFlight scene change");
        SceneManager.LoadScene("SpaceFlight");
    }

    // Opens StartCatcher scene
    public void StartCatcher ()
    {
        Logger.Debug(DEBUG_BUTTON_CLICK, "StartCatcher scene change");
        SceneManager.LoadScene("SpaceCatcher");
    }

    // Opens StartConflict scene
    public void StartConflict ()
    {
        Logger.Debug(DEBUG_BUTTON_CLICK, "StartConflict scene change");
        SceneManager.LoadScene("SpaceConflict");
    }


    // Exits game
    public void ExitGame ()
    {
        Logger.Debug(DEBUG_BUTTON_CLICK, "ExitGame click");
        Application.Quit();
    }
}
