﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConflctController : MonoBehaviour
{

    GameObject ship; // camera of ship
    ConflictSceneManager csm;

    float min_move;
    float speed;

    // Start is called before the first frame update
    void Start()
    {
        //ship = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        ship = gameObject;
        csm = gameObject.GetComponent<ConflictSceneManager>();
        Assert.IsNull(csm);

        min_move = 0.1f;
        speed = 20.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(csm.controls){

            CheckVertical();
            CheckHorizontal();
        }

        CheckConfirm();

        // Shake test
        if (Input.GetKeyDown(KeyCode.T)){

            Shake();
        }
    }


    // Checks vertical inputs
    private void CheckVertical(){

        float movement = Input.GetAxis("Vertical");

        if ((movement > min_move) || (movement < -min_move)) {

            ship.transform.Rotate(Vector3.left * movement * speed * Time.deltaTime);
        }
    }

    // Checks horizontal inputs
    private void CheckHorizontal() {

        float movement = Input.GetAxis("Horizontal");

        if ((movement > min_move) || (movement < -min_move)) {

            ship.transform.Rotate(Vector3.up * movement * speed * Time.deltaTime);
        }
    }

    // Check shoot button
    private void CheckConfirm(){

        bool pressed = Input.GetKey(KeyCode.Space); // TODO: + press external button

        if (pressed) {

            if (csm.weapons) {

                csm.Shoot();

            } else {

                csm.Restart();
            }
        }
    }

    // Shakes camera
    public void Shake(){

        // Camera shake on hit
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        GameObject camera_dock = camera.transform.parent.gameObject;
        StartCoroutine(Alg.CameraShake(camera_dock, 1.0f, 0.1f));

        //Debug.Log(camera_dock.name);
    }
}
