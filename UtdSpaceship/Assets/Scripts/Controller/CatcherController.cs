﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatcherController : MonoBehaviour
{

    float min_move;
    float speed;

    GameManager gm;
    CatcherSceneManager csm;


    // Start is called before the first frame update
    void Start()
    {
        csm = GameObject.FindGameObjectWithTag("Catcher").GetComponent<CatcherSceneManager>();
        Assert.IsNull(csm);

        csm.crosshair = gameObject;
        Assert.IsNull(csm.crosshair);

        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        Assert.IsNull(gm);
        min_move = gm.main_ini.catcher.aim_sensitivity;
        speed = gm.main_ini.catcher.aim_speed;
    }

    // Update is called once per frame
    void Update()
    {
        MovementVertical();
        MovementHorizontal();
        CheckBorders();

        ConfirmButton();
    }


    // Moves crosshair vertically
    private void MovementVertical() {

        float movement = Input.GetAxis("Vertical");

        if((movement > min_move) || (movement < -min_move)) {

            // move up or down
            csm.crosshair.transform.Translate(Vector3.up * speed * Time.deltaTime * movement);
        }
    }

    // Moves crosshair horizontally
    private void MovementHorizontal(){

        float movement = Input.GetAxis("Horizontal");

        if ((movement > min_move) || (movement < -min_move)) {

            // move up or down
            csm.crosshair.transform.Translate(Vector3.right * speed * Time.deltaTime * movement);
        }
    }

    // Handler for confirm/shoot button
    private void ConfirmButton(){

        bool pressed = Input.GetKeyDown(KeyCode.Space); // TODO: + press external button
         
        if (pressed){

            if (csm.game_running && !csm.caught) {

                csm.Shoot();

            } else {

                csm.RestartGame();
            }
        }
    }


    // Checks if crosshair is nto moved from screen borders
    private void CheckBorders(){

        Vector3 position = csm.crosshair.transform.position;

        // vertical max
        if (csm.crosshair.transform.position.y > Screen.height) {

            position.y = Screen.height;
            csm.crosshair.transform.position = position;
        }

        // vertical min
        if (csm.crosshair.transform.position.y < 0.0f) {

            position.y = 0.0f;
            csm.crosshair.transform.position = position;
        }

        // horizontal max
        if (csm.crosshair.transform.position.x > Screen.width) {

            position.x = Screen.width;
            csm.crosshair.transform.position = position;
        }

        // horizontal min
        if (csm.crosshair.transform.position.x < 0.0f) {

            position.x = 0.0f;
            csm.crosshair.transform.position = position;
        }
    }

}
