﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoints : MonoBehaviour
{

    float hit_points;
    bool dead;

    // Start is called before the first frame update
    void Start()
    {
        hit_points = 100.0f;
        dead = false;
    }

    // Update is called once per frame
    void Update()
    {
            
    }


    // Damages ship
    public void TakeDamage(float number){

        hit_points -= number;

        if(hit_points <= 0.0f){

            dead = true;
            Death();
        }
    }

    // Object dies
    private void Death(){

        GameObject explosion = Spawn.SpawnItem("Explosion", "ParticleSystems/", gameObject.transform.position, gameObject.transform.rotation.eulerAngles);
        Spawn.PlaySound("explosion00", "Sounds/Explosions/", 0, explosion.transform.position);
        Destroy(gameObject, 3.0f);
    }
}
