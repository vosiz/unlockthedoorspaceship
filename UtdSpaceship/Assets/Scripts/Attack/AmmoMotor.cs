﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoMotor : MonoBehaviour
{

    const bool LOG_HIT = true;

    float traveled;

    Ammo ammo;
    bool fly;

    // Start is called before the first frame update
    void Start()
    {
        traveled = 0.0f;
        fly = true;
    }

    // Update is called once per frame
    void Update()
    {
        // wait until ammo settings are loaded
        if(ammo != null && fly) {

            float dist = ammo.speed * Time.deltaTime;
            gameObject.transform.Translate(Vector3.forward * dist);

            traveled += dist;
            if (traveled > ammo.max_flight_distance) Destroy();
        } else {

            Debug.Log("Ammo is null");
        }
    }

    // On collision
    private void OnCollisionEnter(Collision collision) {

        enabled = false;
        fly = false;

        GameObject target = collision.gameObject;
        Logger.Debug(LOG_HIT, "Collision with: " + target.name);

        // Add explosion on hit
        Spawn.SpawnItem(ammo.hit_effect_name, ammo.hit_effect_path, gameObject.transform.position, gameObject.transform.rotation.eulerAngles);
        Spawn.PlaySound(ammo.hit_sound_name, ammo.hit_sound_path, 0, Vector3.zero, 0.3f);

        // Add force
        //target.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * 20.0f, collision.GetContact(0).point);

        // Do damage
        HitPoints damage_module = target.GetComponent<HitPoints>();
        if(damage_module != null){

            damage_module.TakeDamage(ammo.damage);

            // Set target on fire
            GameObject fire = Spawn.SpawnItem("OnFire", "ParticleSystems/", collision.GetContact(0).point, gameObject.transform.rotation.eulerAngles);
            fire.transform.SetParent(target.transform);
        }

        
        if (target.name == "PlayerShip") {

            target.GetComponent<ConflctController>().Shake();
        }

        Destroy();
    }


    // Loads settings to projectil
    public void SetAmmo(Ammo ammo){

        this.ammo = ammo;
    }

    // Destroys ammo with effect
    private void Destroy(){

        Destroy(gameObject);
    }
}
