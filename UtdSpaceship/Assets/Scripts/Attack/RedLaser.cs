﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLaser : Ammo
{
    public RedLaser() : base(AppConfig.ammo.red_laser.ID) {

        speed = AppConfig.ammo.red_laser.SPEED;
        max_flight_distance = AppConfig.ammo.red_laser.FLIGHT_DISTANCE;

        hit_sound_path = "Sounds/Explosions/";
        hit_sound_name = "explosion00";

        reload_time = AppConfig.ammo.red_laser.REALOAD_TIME;
        damage = AppConfig.ammo.red_laser.DAMAGE;
    }
}
