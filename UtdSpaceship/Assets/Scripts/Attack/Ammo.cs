﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo
{
    public string ammoname;
    public float speed;
    public float max_flight_distance;
    public float reload_time;
    public float damage;

    // by default
    public string shot_name;
    public string shot_path;
    public string hit_effect_name;
    public string hit_effect_path;
    public string hit_sound_name;
    public string hit_sound_path;
    

    public Ammo(string name){

        ammoname = name;

        hit_effect_path = "ParticleSystems/Hits/";
        hit_sound_path = "Sounds/Hits/";
        shot_path = "Sounds/Shots/";

        hit_effect_name = name + "Hit";
        hit_sound_name = name + "Hit";
        shot_name = name + "Shot";
    }

}
