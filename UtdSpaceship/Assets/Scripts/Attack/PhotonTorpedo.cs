﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonTorpedo : Ammo
{
    
    public PhotonTorpedo() : base(AppConfig.ammo.photon_torpedo.ID){

        speed = AppConfig.ammo.photon_torpedo.SPEED;
        max_flight_distance = AppConfig.ammo.photon_torpedo.FLIGHT_DISTANCE;

        hit_sound_path = "Sounds/Explosions/";
        hit_sound_name = "explosion00";

        reload_time = AppConfig.ammo.photon_torpedo.REALOAD_TIME;
        damage = AppConfig.ammo.photon_torpedo.DAMAGE;
    }
}
