﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moon : MonoBehaviour
{
    readonly float PLANET_ROTATION_SPEED = 1.1f;
    readonly float APPROACH_SPEED = 1000.0f;

    Transform moon;

    public Camera cam;

    // Start is called before the first frame update
    void Start() {

        moon = gameObject.transform;
    }

    // Update is called once per frame
    void Update() {

        Rotate();

        // Approach only if not in range already
        if (!VectorUtils.IsInRange(
            gameObject.transform.position,
            cam.transform.position,
            50.0f)) {

            Approach();
        } 
    }


    // Rotates planet and clouds
    private void Rotate() {

        moon.Rotate(Vector3.up * PLANET_ROTATION_SPEED * Time.deltaTime);
    }

    // Pseudoapproaches player´s camera
    private void Approach(){

        VectorUtils.MoveTowardsPoint(gameObject, cam.transform.position, APPROACH_SPEED * Time.deltaTime);
    }
}
