﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    readonly float PLANET_ROTATION_SPEED = 0.8f;
    readonly float CLOUDS_ROTATION_SPEED = 0.6f;

    Transform planet;
    Transform clouds;

    // Start is called before the first frame update
    void Start()
    {
        GameObject go;

        go = Utils.GetGameObjectByName(gameObject, "planet");
        Assert.IsNull(go);
        planet = go.transform;

        go = Utils.GetGameObjectByName(gameObject, "clouds");
        Assert.IsNull(go);
        clouds = go.transform;
    }

    // Update is called once per frame
    void Update()
    {

        Rotate();
    }


    // Rotates planet and clouds
    private void Rotate(){

        planet.Rotate(Vector3.up * PLANET_ROTATION_SPEED * Time.deltaTime);
        clouds.Rotate(Vector3.up * CLOUDS_ROTATION_SPEED * Time.deltaTime);
    }
}
