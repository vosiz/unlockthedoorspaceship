﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WormholeExit : MonoBehaviour
{

    ParticleSystem effect;
    GameObject hyperspace;
    Camera cam;
    float timer;
    bool exiting;
    bool entering;

    Vector3 default_pos;

    // 57,3

    // Start is called before the first frame update
    void Start()
    {

        hyperspace = Utils.GetGameObjectByName(gameObject, "Stars");
        effect = hyperspace.GetComponent<ParticleSystem>();

        cam = gameObject.GetComponent<Camera>();

        timer = 0.0f;
        exiting = false;
        entering = true;

        default_pos = hyperspace.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        if (entering) EnterHyperspace();

        if (Input.GetKeyDown(KeyCode.Space)) {

            exiting = true;
            entering = false;
            cam.fieldOfView = 15.0f;
            cam.clearFlags = CameraClearFlags.Skybox;
            effect.Stop();
        }

        if (exiting) ExitHyperspace();
    }

    
    // Enter hyperspace effect
    private void EnterHyperspace(){

        if (cam.fieldOfView > 15.0f) {

            if (timer > 0.01f) {

                timer = 0.0f;
                cam.fieldOfView -= 1.5f;
            }
        } else {

            entering = false;
        }
    }

    // Exit hyperspace effect
    private void ExitHyperspace(){

        if(cam.fieldOfView < 60.0f){

            if (timer > 0.01f) {

                timer = 0.0f;
                cam.fieldOfView += 1.5f;
            }
        } else {

            if (timer > 3.0f) {

                SceneManager.LoadScene("SpaceFlight");
            }
        }
    }
}
