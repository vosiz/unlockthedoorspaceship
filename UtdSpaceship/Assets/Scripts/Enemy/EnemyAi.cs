﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyAiPhase{

    WARP_IN,
    WAIT,
    ROTATE,
    ORBIT,
}

public class EnemyAi : MonoBehaviour
{

    const float AFTER_WARP_WAIT = 3.5f;
    const float TURNING_SHIP = 10.0f;
    const float ATTACK_PLAYER = 3.0f;

    EnemyAiPhase phase;

    float timer;

    // Orbit
    Transform center;
    Vector3 axis;
    float rotation_speed, radius, radius_speed;
    Vector3 ship_rotation;

    // Start is called before the first frame update
    void Start()
    {

        phase = EnemyAiPhase.WARP_IN;
        timer = 0.0f;

        Utils.GetGameObjectByName(gameObject, "Model").SetActive(false);

        center = FindObjectOfType<Camera>().transform;
        axis = Vector3.down;
        rotation_speed = 1.0f;
        radius = 2.0f;
        radius_speed = 0.5f;

        ship_rotation = new Vector3(-7.0f, 60.0f, 20.0f);
        //gameObject.transform.rotation = Quaternion.Euler(ship_rotation);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        CheckPhase();
    }


    // Checks current phase and do things
    private void CheckPhase(){

        switch(phase){

            case EnemyAiPhase.WARP_IN:
                WarpIn();
                break;

            case EnemyAiPhase.WAIT:
                WaitWarps();
                break;

            case EnemyAiPhase.ROTATE:
                RotateShip();
                break;

            case EnemyAiPhase.ORBIT:
                OrbitPlayer();
                break;

            default:
                throw new System.Exception("Unknown state: " + phase.ToString());
        }
    }

    // Warps ship in
    private void WarpIn(){

        Spawn.SpawnItem("HyperspaceExit", "ParticleSystems/", gameObject.transform.position + Vector3.forward * 20.0f, Vector3.zero);
        Spawn.PlaySound("hyperspace_exit", "Sounds/World/", 0, FindObjectOfType<Camera>().transform.position, 0.4f);

        timer = 0.0f;
        phase = EnemyAiPhase.WAIT;
    }

    // Wait until all warps are done (timer)
    private void WaitWarps(){

        timer += Time.deltaTime;
        if(timer >= AFTER_WARP_WAIT){

            timer = 0.0f;
            phase = EnemyAiPhase.ROTATE;

            Utils.GetGameObjectByName(gameObject, "Model").SetActive(true);
        }
    }

    // Rotates ship to attack position
    private void RotateShip(){

        gameObject.transform.Rotate(ship_rotation * 1/10 * Time.deltaTime);

        if (timer > TURNING_SHIP){

            phase = EnemyAiPhase.ORBIT;
            timer = 0.0f;
        }
        
    }

    // Orbits player with given settings
    private void OrbitPlayer(){

        transform.RotateAround(center.position, axis, rotation_speed * Time.deltaTime);

        Vector3 desiredPosition = (transform.position - center.position).normalized * radius + center.position;

        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radius_speed);

        if(timer > ATTACK_PLAYER){

            timer = 0.0f;
            Attack();
        }
    }

    // Attack Player with laser
    private void Attack(){

        const float DEVIATION = 0.7f;

        // Random number as deviation from players location (2,5m wide target)
        float x, y, z;
        x = Random.Range(-DEVIATION, DEVIATION);
        y = Random.Range(-DEVIATION, DEVIATION);
        z = Random.Range(-DEVIATION, DEVIATION);

        Vector3 target_coords = new Vector3(x, y, z);
        target_coords += FindObjectOfType<Camera>().transform.position;
        //Vector3 vector = Vector3.zero - target_coords;

        // Spawn laser and play sound
        GameObject projectile = Spawn.SpawnItem("RedLaser", "Models/Projectile/", gameObject.transform.position, Vector3.zero);
        VectorUtils.RotateTo(projectile, target_coords);
        //projectile.transform.Rotate(Vector3.up * 90);

        projectile.GetComponent<AmmoMotor>().SetAmmo(new RedLaser());

        Spawn.PlaySound("RedLaser", "Sounds/Shots/", 0, Vector3.zero, 0.6f);

    }
}
