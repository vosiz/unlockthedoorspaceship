﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum CatcherTargetState{

    NORMAL,
    TARGETING,
    CAUGHT,
}

public class CatcherGui : MonoBehaviour
{

    float blink_timer;
    float blink_frequency;
    bool blinked;
    public bool targeting;

    CatcherTargetState state;

    GameObject overlayer;
    GameObject target;
    GameObject up_arrow;
    GameObject down_arrow;
    GameObject left_arrow;
    GameObject right_arrow;

    Image target_sprite;

    GameManager gm;

    // Start is called before the first frame update
    void Start() {

        overlayer = Utils.GetGameObjectByName(gameObject, "Overlayer");
        Assert.IsNull(overlayer);

        target = GameObject.FindGameObjectWithTag("CatcherTarget");
        Assert.IsNull(target);
        target_sprite = target.GetComponent<Image>();
        Assert.IsNull(target_sprite);

        GameObject crosshair = Utils.GetGameObjectByName(gameObject, "Crosshair");
        Assert.IsNull(crosshair);
        up_arrow = Utils.GetGameObjectByName(crosshair, "UpArrow");
        Assert.IsNull(up_arrow);
        down_arrow = Utils.GetGameObjectByName(crosshair, "DownArrow");
        Assert.IsNull(down_arrow);
        left_arrow = Utils.GetGameObjectByName(crosshair, "LeftArrow");
        Assert.IsNull(left_arrow);
        right_arrow = Utils.GetGameObjectByName(crosshair, "RightArrow");
        Assert.IsNull(right_arrow);

        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        Assert.IsNull(gm);
        blink_frequency = gm.main_ini.catcher.target_blink_freq;

        blink_timer = 0.0f;
        targeting = false;
        blinked = false;

        HideArrows();
    }

    // Update is called once per frame
    void Update()
    {
        if (targeting){

            Blink();
        }
    }


    // Check if target shoudl blink -> change its image
    private void Blink(){

        blink_timer += Time.deltaTime;
        if(blink_timer > blink_frequency){

            if(blinked){

                ChangeTargetImage("DarkGreenArrow");

            } else {

                ChangeTargetImage("GreenArrow");
            }

            blinked = !blinked;
            blink_timer -= blink_frequency;
        }
    }

    // Show or hides overlayer
    public void ShowOverlayer(bool show){

        overlayer.SetActive(show);
    }

    // Set state of targeting of falling target, affects its appearance
    public void SetTargetingState(CatcherTargetState set) {

        targeting = false;

        switch(set){

            case CatcherTargetState.CAUGHT:
                ChangeTargetImage("BlueArrow");
                break;

            case CatcherTargetState.TARGETING:

                targeting = true;

                break;

            default:
                ChangeTargetImage("RedArrow");
                break;
        }
    }

    // Change image of target
    private void ChangeTargetImage(string imagename){

        target_sprite.sprite = Resources.Load<Sprite>("Sprites/Catcher/" + imagename);
    }


    // Hides all arrows
    public void HideArrows() {

        up_arrow.SetActive(false);
        down_arrow.SetActive(false);
        left_arrow.SetActive(false);
        right_arrow.SetActive(false);
    }

    // Shows up-arrow
    public void ShowUpArrow(){

        up_arrow.SetActive(true);
    }

    // Shows down-arrow
    public void ShowDownArrow() {

        down_arrow.SetActive(true);
    }

    // Shows left-arrow
    public void ShowLeftArrow() {

        left_arrow.SetActive(true);
    }

    // Shows right-arrow
    public void ShowRightArrow() {

        right_arrow.SetActive(true);
    }
}
