﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;





public class ConflictGui : MonoBehaviour
{
    const float FADING_TIME = 2.0f;

    GameObject overlayer;
    Image overlayer_sprite;
    Color default_sprite_color;

    GameObject crossahair;

    bool overlayer_active;
    float overlayer_timer;
    public bool foo;

    public Text engines;
    public Text mode;
    public Text weapons_sys;


    // Start is called before the first frame update
    void Start()
    {
        overlayer = Utils.GetGameObjectByName(gameObject, "Overlayer");
        Assert.IsNull(overlayer);
        Assert.IsNull(overlayer.GetComponent<Image>());
        overlayer_sprite = overlayer.GetComponent<Image>();
        default_sprite_color = overlayer_sprite.color;

        overlayer_timer = 0.0f;
        overlayer_active = false;

        Text[] texts = FindObjectsOfType<Text>();
        foreach(Text t in texts){

            t.color = AppConfig.colors.TEXT_GUI_NEUTRAL;
        }

        GameObject go;
        go = Utils.GetGameObjectByName(gameObject, "Engines");
        Assert.IsNull(go);
        engines = go.GetComponent<Text>();
        Assert.IsNull(engines);

        go = Utils.GetGameObjectByName(gameObject, "Mode");
        Assert.IsNull(go);
        mode = go.GetComponent<Text>();
        Assert.IsNull(mode);

        go = Utils.GetGameObjectByName(gameObject, "WeaponSystem");
        Assert.IsNull(go);
        weapons_sys = go.GetComponent<Text>();
        Assert.IsNull(weapons_sys);

        crossahair = Utils.GetGameObjectByName(gameObject, "Crosshair");
        Assert.IsNull(crossahair);
    }

    // Update is called once per frame
    void Update()
    {

        if (overlayer_active){

            OverlayerFading();
        }
    }


    // Show or hides overlayer
    public void ShowOverlayer(bool show){

        overlayer_active = show;
        overlayer.SetActive(show);
    }

    // Fades out when hit by enemy
    private void OverlayerFading(){

        // first call, first frame of fading
        if(overlayer_timer >= FADING_TIME){

            overlayer_timer = 0.0f;
            ShowOverlayer(false);
        }

        overlayer_timer += Time.deltaTime;

        float perc = default_sprite_color.a * overlayer_timer / FADING_TIME;
        Color nucolor = default_sprite_color;
        nucolor.a = default_sprite_color.a - perc;
        overlayer_sprite.color = nucolor;
    }

    // Shows crosshair
    public void ShowCrosshair(bool show) {

        crossahair.SetActive(show);
    }

}
