﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppConfig
{
    static public AppInfo appinfo = new AppInfo();
    
    static public LimitConfig limits = new LimitConfig();
    static public ColorConfig colors = new ColorConfig();

    static public NameConfig names = new NameConfig();
    static public PathConfig paths = new PathConfig();

    static public AmmoConfig ammo = new AmmoConfig();
}
