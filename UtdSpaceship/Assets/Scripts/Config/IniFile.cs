﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Runtime.InteropServices;

public enum IniFilesId {

    MAIN,
}

public class IniFile
{

    static private readonly bool LOG_ALL = false; // log whole class
    private readonly bool LOG_EXIST = true & LOG_ALL;
    private readonly bool LOG_CREATE = true & LOG_ALL;
    private readonly bool LOG_READ = true & LOG_ALL;
    private readonly bool LOG_WRITE = true & LOG_ALL;

    protected IniFilesId id;


    // External function for ini file write
    [DllImport("kernel32")]
    private static extern long WritePrivateProfileString(string section, string key, string val, string filepath);

    // Externa function for ini file read
    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filepath);


    public IniFile(IniFilesId id) {

        this.id = id;
    }


    // Checks if ini file exists or not
    // Returns true if exists
    protected bool Exists() {

        string path = GetPath(id);
        bool e = System.IO.File.Exists(path);

        Logger.Debug(LOG_EXIST, "File id " + id.ToString() + " exists: " + e.ToString());

        return e;
    }

    // Creates default ini files
    protected void Create() {

        string path = GetPath(id);
        System.IO.File.Create(path);

        Logger.Debug(LOG_CREATE, "Creating file id " + id.ToString() + " on path " + path);
    }


    // Gets path of ini file ID
    private string GetPath(IniFilesId id){

        switch(id){

            case IniFilesId.MAIN:
                return AppConfig.paths.MAIN_INI_FILE_PATH;

            default:
                throw new System.Exception("Unknown ini file ID! " + id.ToString());
        }
    }


    // Writes value to ini file
    public void WriteValue(string section, string key, string val){

        string path = GetPath(id);

        long o = WritePrivateProfileString(section, key, val, path);
        Logger.Debug(LOG_WRITE, "Writing " + val + " to section " +section + " of key " + key + " on path " + path + " code: " + o);
    }

    // Read value from ini file
    public string ReadValue(string section, string key){

        StringBuilder temp = new StringBuilder(AppConfig.limits.BUFFER_SIZE_INI_FILES);
        string path = GetPath(id);

        Logger.Debug(LOG_READ, "Attempt to read key" + key + " of section " + section + " on path " + path);
        GetPrivateProfileString(
            section, key, "", temp, AppConfig.limits.BUFFER_SIZE_INI_FILES, path);

        string output = temp.ToString();
        if(output.Length == 0) {

            string excp_string = string.Format("There is no value in section {0} of key {1} in ini file on path {2}", section, key, path);
            throw new System.Exception(excp_string);
        }

        return output;
    }

    // Throws exception when it is time to panic
    public void Panic(string text){

        throw new System.Exception(text);
    }
}
