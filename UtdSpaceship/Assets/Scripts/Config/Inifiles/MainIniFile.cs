﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SectionCatcher {

    public float aim_sensitivity;
    public float aim_speed;
    public float target_speed;
    public float dot_frequency;
    public float target_blink_freq;
    public float target_screen_offset;
};

public class MainIniFile : IniFile
{
    const string PANIC_HEADER = "MainIni file inconsistency: ";

    public SectionCatcher catcher;


    public MainIniFile() : base(IniFilesId.MAIN) {

        if (!Exists()) CreateDefaultMain();
        LoadValues();
    }

    // Writes default values to Main ini file
    private void CreateDefaultMain() {

        // Catcher scene
        WriteValue("Catcher", "AimSensitivity", "0,1");
        WriteValue("Catcher", "AimSpeed", "100");
        WriteValue("Catcher", "TargetSpeed", "20");
        WriteValue("Catcher", "DotFrequency", "1,2");
        WriteValue("Catcher", "TargetBlinkFrequency", "0,3");
        WriteValue("Catcher", "TargetScreenOffset", "20");
    }

    // Loads all values from Main ini file
    private void LoadValues() {

        string value;

        catcher = new SectionCatcher();

        value = ReadValue("Catcher", "AimSensitivity");
        if (!float.TryParse(value, out catcher.aim_sensitivity)) Panic(PANIC_HEADER + "AimSensitivity");

        value = ReadValue("Catcher", "AimSpeed");
        if (!float.TryParse(value, out catcher.aim_speed)) Panic(PANIC_HEADER + "AimSpeed");

        value = ReadValue("Catcher", "TargetSpeed");
        if (!float.TryParse(value, out catcher.target_speed)) Panic(PANIC_HEADER + "TargetSpeed");

        value = ReadValue("Catcher", "DotFrequency");
        if (!float.TryParse(value, out catcher.dot_frequency)) Panic(PANIC_HEADER + "DotFrequency");

        value = ReadValue("Catcher", "TargetBlinkFrequency");
        if (!float.TryParse(value, out catcher.target_blink_freq)) Panic(PANIC_HEADER + "TargetBlinkFrequency");

        value = ReadValue("Catcher", "TargetScreenOffset");
        if (!float.TryParse(value, out catcher.target_screen_offset)) Panic(PANIC_HEADER + "TargetScreenOffset");
    }
}
