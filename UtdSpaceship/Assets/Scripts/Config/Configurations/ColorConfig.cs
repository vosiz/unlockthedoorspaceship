﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorConfig
{

    public readonly Color TEXT_GUI_NEUTRAL = Color.white;
    public readonly Color TEXT_GUI_ACTIVE = Color.green;
    public readonly Color TEXT_GUI_DISABLED = Color.red;
    public readonly Color TEXT_GUI_ALERT = Color.red;
    public readonly Color TEXT_GUI_ERROR = Color.red;


    public ColorConfig() { }
}
