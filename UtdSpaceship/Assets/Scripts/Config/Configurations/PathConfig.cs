﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathConfig {

    // Configuration paths
    public readonly string MAIN_INI_FILE_PATH = 
        Application.dataPath + "/" + AppConfig.names.MAIN_INI_FILE_NAME;


    // Constructor
    public PathConfig(){ }
}
