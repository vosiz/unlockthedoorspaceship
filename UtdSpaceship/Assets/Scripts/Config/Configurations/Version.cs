﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Version
{

    public Version(){ }

    // Returns string of version
    // returns: string in format Major.Minor.Patch
    public string GetVersion(){

        return string.Format("{0}.{1}.{2}", 
            AppConfig.appinfo.VERSION_MAJOR,
            AppConfig.appinfo.VERSION_MINOR,
            AppConfig.appinfo.VERSION_PATCH);
    }

    // Returns string of version
    // returns: string in format Major.Minor.Patch-DevState
    public string GetVersionDevel() {

        return string.Format("{0}-{1}", GetVersion(), DevelToString(AppConfig.appinfo.state));
    }

    // Returns full string of version
    // returns: string in format Major.Minor.Patch-DevState.Os.Arch.Revision
    public string GetFullVersion() {

        return string.Format("{0}.{1}.{2}.{3}",
            GetVersionDevel(),
            OsToString(AppConfig.appinfo.os),
            ArchToString(AppConfig.appinfo.arch),
            AppConfig.appinfo.BUILD_REVISION);
    }


    // Converts DevelState to string interpretation
    private string DevelToString(DevelopmentState state){

        switch(state){

            case DevelopmentState.DEVEL:
                return "dev";
            case DevelopmentState.RELEASE:
                return "rel";

            default:
                throw new System.Exception();
        }
    }

    // Converts OperationSystem to string interpretation
    private string OsToString(OperationSystem os) {

        switch (os) {

            case OperationSystem.WIN:
                return "win32";
            case OperationSystem.LINUX:
                return "linux";
            case OperationSystem.MAC:
                return "mac";
            case OperationSystem.TRIO:
                return "win32_linux_mac";
            case OperationSystem.ANDROID:
                return "adnroid";

            default:
                throw new System.Exception();
        }
    }

    // Converts Architecture to string interpretation
    private string ArchToString(Architecture arch) {

        switch (arch) {

            case Architecture.X86:
                return "x86";
            case Architecture.X64:
                return "x64";

            default:
                throw new System.Exception();
        }
    }
}
