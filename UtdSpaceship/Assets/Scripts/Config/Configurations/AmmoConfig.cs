﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoConfig
{

    public PhotonTorpedoConfig photon_torpedo;
    public RedLaserConfig red_laser;


    public AmmoConfig() {

        photon_torpedo = new PhotonTorpedoConfig();
        red_laser = new RedLaserConfig();
    }
}
