﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonTorpedoConfig
{
    // Clissification
    public readonly string ID = "PhotonTorpedo";

    // Attack abilities
    public readonly float SPEED = 60.0f;
    public readonly float FLIGHT_DISTANCE = 500.0f;
    public readonly float REALOAD_TIME = 2.0f;
    public readonly float DAMAGE = 25.0f;

    public PhotonTorpedoConfig() { }
}
