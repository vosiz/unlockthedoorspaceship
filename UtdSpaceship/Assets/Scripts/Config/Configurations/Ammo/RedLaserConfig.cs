﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLaserConfig
{
    // Clissification
    public readonly string ID = "RedLaser";

    // Attack abilities
    public readonly float SPEED = 60.0f;
    public readonly float FLIGHT_DISTANCE = 500.0f;
    public readonly float REALOAD_TIME = 4.0f;
    public readonly float DAMAGE = 25.0f;

    public RedLaserConfig() { }
}
