﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DevelopmentState {

    DEVEL,
    RELEASE,
}

public enum OperationSystem {

    WIN,
    LINUX,
    MAC,
    TRIO,
    ANDROID,
}

public enum Architecture {

    X86,
    X64,
}

public class AppInfo {

    public readonly int VERSION_MAJOR = 0;
    public readonly int VERSION_MINOR = 1;
    public readonly int VERSION_PATCH = 0;
    public readonly long BUILD_REVISION = 201901020013;

    public readonly DevelopmentState state = DevelopmentState.DEVEL;

    public readonly OperationSystem os = OperationSystem.WIN;
    public readonly Architecture arch = Architecture.X64;

    public AppInfo() { }
}
